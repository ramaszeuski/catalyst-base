FROM debian:buster-20220801-slim

RUN mkdir /var/opt/app; chown daemon /var/opt/app; apt-get update && apt-get install -y \
 curl \
 libcatalyst-controller-actionrole-perl \
 libcatalyst-engine-apache-perl \
 libcatalyst-modules-perl \
 libcatalyst-perl \
 libcatalyst-plugin-log-dispatch-perl \
 libcatalyst-plugin-unicode-encoding-perl \
 libcatalyst-view-tt-perl \
 libcrypt-blowfish-perl \
 libcrypt-cbc-perl \
 libdate-manip-perl \
 libdate-simple-perl \
 libdatetime-format-http-perl \
 libdatetime-format-pg-perl \
 libdatetime-set-perl \
 libdbd-pg-perl \
 libdbix-class-perl \
 libfcgi-procmanager-perl \
 libhtml-formfu-model-dbic-perl \
 libhtml-formfu-perl \
 liblist-compare-perl \
 libmath-round-perl \
 libmoose-perl \
 libspreadsheet-writeexcel-perl \
 libtext-csv-perl \
 libyaml-syck-perl

EXPOSE 3000
WORKDIR /opt/app
ENV PERL5LIB=/opt/app/lib CATALYST_CONFIG=/var/opt/app/config.yaml CATALYST_HOME=/opt/app CATALYST_DEBUG=1

